package anon.trollegle;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.WeakHashMap;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;

public class Multi implements Callback {

	private List<UserConnection> users = new Vector<>();
	private List<UserConnection> informed = new Vector<>();
	private List<UserConnection> welcomed = new Vector<>();
	
	private List<CaptchaDialog> captchas = new Vector<>();
	private Map<UserConnection, CaptchaDialog> userCaptchas = new WeakHashMap<>();
	private boolean captchasPublic;
	
	private Map<UserConnection, Set<UserConnection>> kickVotes = new HashMap<>();
	
	private Set<String> deduper = new HashSet<>();
	private Set<String> bans = new HashSet<>();
	
	private int joinFreq = 5000, pulseFreq = 360007, maxChatting = 9, maxTotal = 25;
	private int inactiveBot = 70000, inactiveHuman = 600000;
	private double qfreq = 0.97;
	private boolean autoJoin;
	private long lastInvite = System.currentTimeMillis();
	private long lastPulse = System.currentTimeMillis();
	private int minInterval = 3000;
	private boolean murder;
	
	public Multi() {
		new Thread() {
			private ArrayDeque<UserConnection> targets = new ArrayDeque<>();
			
			public void run() {
				while (true) try {
					Thread.sleep(joinFreq() / 2 + (int) (Math.random() * joinFreq()));
					
					int pulses = 0;
					int zombies = 0;
					
					synchronized(users) {
						for (UserConnection u : users) {
							if (u.isPulse()) {
								if (u.idleFor() > pulseFreq) {
									u.sendPulse(false);
									u.unpulse(autoJoin && Math.random() * (maxChatting + 1) > welcomed.size());
								} else {
									pulses++;
									u.pingStatus();
								}
								continue;
							}
							synchronized(welcomed) {
								if ((!u.isAccepted() || !welcomed.contains(u)) && u.idleFor() > inactiveBot ||
										u.idleFor() > inactiveHuman * (murder && !autoJoin ? 3 : 1))
									targets.add(u);
							}
							if (u.idleFor() > inactiveBot && !u.isConnected() && !u.isPulseEver())
								zombies++;
						}
					}
					while (!targets.isEmpty()) {
						kickInactive(targets.pop());
					}
					if (zombies > 2)
						banned();
					
					if (autoJoin && welcomed.size() < maxChatting && users.size() < maxTotal)
						add();
					
					if (pulseFreq >= 0 && pulses == 0 && System.currentTimeMillis() - lastPulse >= pulseFreq) {
						addPulse();
						lastPulse = System.currentTimeMillis();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
	
	public void banned() {
		System.out.println("Banned! Spam and pulse off");
		System.out.println("(Freqs until now: spam " + joinFreq + " pulse " + pulseFreq);
		autoJoin = false;
		pulseFreq = -1;
		murder = true;
		tellRoom("We've been hard banned! No new users can join. Have fun watching the chat die off.");
	}
	
	public void ban(String expr) {
		synchronized(bans) {
			bans.add(expr);
		}
	}
	
	public String[] getBans() {
		synchronized(bans) {
			return bans.toArray(new String[bans.size()]);
		}
	}
	
	public void clearBans() {
		synchronized(bans) {
			bans.clear();
		}
	}
	
	public int joinFreq() {
		return welcomed.size() == 0 ? joinFreq : joinFreq * welcomed.size() * welcomed.size();
	}
	
	public UserConnection userFromName(String name) {
		synchronized(users) {
			for (UserConnection u : users)
				if (name.equals(u.getNick()))
					return u;
		}
		return null;
	}
	
	public void tellRoom(String message) {
		tellRoom(message, null);
	}
	public void tellRoom(String message, UserConnection excluded) {
		synchronized(users) {
			for (UserConnection u : users)
				if (u.isReady() && u != excluded)
					u.schedTell(message);
		}
	}
	
	public void voteKick(UserConnection nuisance, UserConnection saviour) {
		synchronized(kickVotes) {
			if (!kickVotes.containsKey(nuisance))
				kickVotes.put(nuisance, new HashSet<UserConnection>());
			kickVotes.get(nuisance).add(saviour);
		}
		try {
			if (kickVotes.get(nuisance).size() * 1.0 / (welcomed.size() - 1) > 0.65)
				kickVoted(nuisance);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void unvoteKick(UserConnection nuisance, UserConnection saviour) {
		synchronized(kickVotes) {
			if (!kickVotes.containsKey(nuisance))
				return;
			kickVotes.get(nuisance).remove(saviour);
		}
	}
	
	public void addPulse() {
		UserConnection uc = new UserConnection(this, true, false, deduper);
		uc.pulse();
		synchronized(users) {
			uc.start();
			users.add(uc);
		}
	}
	public boolean add(Boolean forceQ) {
		boolean isQ = forceQ == null ? false : forceQ || Math.random() < (qfreq);
		if (!isQ) {
			int texts = 0;
			synchronized(users) {
				for (UserConnection u : users)
					if (!u.isQuestionMode() && !u.isPulse())
						texts++;
			}
			if (texts != 0)
				isQ = true;
		}
		UserConnection uc = new UserConnection(this, isQ, isQ, deduper);
		synchronized(users) {
			uc.start();
			users.add(uc);
		}
		return isQ;
	}
	public boolean add() {
		return add(false);
	}
	
	private void remove(UserConnection user) {
		if (user == null) {
			System.err.println("warn: removing null user!");
			return;
		}
		user.dispose();
		synchronized(users) {
			users.remove(user);
		}
		synchronized(welcomed) {
			welcomed.remove(user);
		}
		synchronized(informed) {
			informed.remove(user);
		}
		if (user.didLive())
			tellRoom(user.getNick() + " has left");
		if (users.size() == 0 && !murder) {
			try {
				Thread.sleep(10000);
				add();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		} else if (users.size() == 1 && !murder) {
			synchronized(users) {
				for (UserConnection u : users)
					u.schedTell("You are the only one left. We'll try to get you another stranger.");
			}
			try {
				Thread.sleep(2000);
				add();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		
		synchronized(kickVotes) {
			kickVotes.remove(user);
			for (Set<UserConnection> subset : kickVotes.values())
				subset.remove(user);
		}
	}
	
	private void typing(UserConnection user) {
		if (user.isAccepted())
			synchronized(users) {
				for (UserConnection u : users)
					if (u != user && u.isReady())
						u.schedSendTyping();
			}
	}
	
	private void hear(UserConnection user, String data) {
		if (user.getMsgCount() < 3) {
			if (data.startsWith("| ") && (data.contains(" There are ") || data.contains(" are in the "))
				|| data.contains("dropbox") || data.contains("blasze") || data.contains("omegle69")
				|| data.contains("Hello beauty! :)") || data.contains("sexy female here u?")
				|| data.contains("kera.pm") || data.matches("^(1[89]|2[0-5]) [fF]") && data.length() < 20
				|| data.contains("io-chat") || data.contains("ha.do/") || data.contains("jmp2.in/")
				|| data.toLowerCase().contains("svetlana") && data.toLowerCase().contains("interest")
			) {
				kickBot(user);
				return;
			}
			for (String expr : bans)
				if (data.matches(expr)) {
					kickBot(user);
					return;
				}
		}
		if (!user.isAccepted())
			if (user.getMsgCount() > 1)
				kickBot(user);
			else
				user.schedTell("To enter the room, you need to say \"toothpaste\".");
		else if (data.startsWith("/"))
			command(user, data);
		else
			synchronized(users) {
				for (UserConnection u : users)
					if (u != user && u.isAccepted())
						u.schedTell(user.getNick(), data);
			}
					
	}
	private void command(UserConnection user, String data) {
		String[] ca = data.split(" ", 2);
		if (ca[0].equalsIgnoreCase("/help")) {
			user.schedTell("Here are all the commands I accept.\n" +
				"/invite\n" + 
				"    Adds another stranger to the chat (" + 100 * qfreq + "% recruited from question mode)\n" +
				"/msg USER MESSAGE\n" +
				"    Sends a private message to the user\n" +
				"/me MESSAGE\n" +
				"    Sends an irc-like action message\n" +
				"/nick NAME\n" +
				"    Changes your name to the specifed NAME\n" +
				"/list\n" +
				"    Shows who's in the chat\n" +
				"/kick NAME\n" +
				"    Votes for the user to be kicked. (2/3 of the room needs to do this for the kick to happen.)\n" +
				"/spam\n" +
				"    Starts looking for new strangers (one every " + joinFreq/1000 + "*num_users^2 secs)\n" +
				"/nospam\n" +
				"    Stops looking for new strangers\n" +
				"    (Spamming is currently " + (autoJoin ? "on" : "off") + ".)\n" +
				"/fuckyoubecause MESSAGE\n" +
				"    Sends a message to the administrator of this system\n" +
				"Curious about how this works? Download teh codes at http://gitlab.com/jtrygva/trollegle\n" +
				"Want to leave and rejoin later? See " + UserConnection.MOTHERSHIP
			);
		} else if (ca[0].equalsIgnoreCase("/id")) {
			String pulse = "none";
			String randid = user.getNick() + " (" + (user.isPulseEver() ? user.getPulseWords() : 
				user.isQuestionMode() ? user.getQuestion() : "text mode") + ", " + user.randid + ")";
			synchronized(users) {
				for (UserConnection u : users)
					if (u.isPulse()) {
						pulse = u.getNick() + " (" + u.getPulseWords() + ", " + u.randid + ", " + u.idleFor() / 1000 + " s old)";
						break;
					}
			}
			user.schedTell("You: " + randid + 
				"\nPulse connection: " + pulse +
				"\nMothership: " + UserConnection.MOTHERSHIP
			);
		} else if (ca[0].equalsIgnoreCase("/invite")) {
			if (System.currentTimeMillis() - lastInvite < minInterval) {
				user.schedTell("Too fast! Try again in a few seconds.");
			} else if (users.size() >= maxTotal || welcomed.size() >= maxChatting) {
				user.schedTell("Chat room is full.");
			} else {
				lastInvite = System.currentTimeMillis();
				if (add())
					tellRoom(user.getNick() + " has invited a new user");
				else
					tellRoom(user.getNick() + " has invited a new user (requesting toothpaste - hold on)");
			}
		} else if (ca[0].equalsIgnoreCase("/msg")) {
			String[] p = ca[1].split(" ", 2);
			System.out.println("Message command components: " + Arrays.toString(p));
			if (p.length == 2) {
				UserConnection dest = userFromName(p[0]);
				if (dest == null) {
					user.schedTell("No such user - type /list to see who's online.");
				} else {
					dest.schedTell("(private) " + user.getNick(), p[1]);
					user.schedTell("message sent");
				}
			} else {
				user.schedTell("You need to give me both a user and a message. Syntax: /msg USER MESSAGE");
			}
		} else if (ca[0].equalsIgnoreCase("/me")) {
			if (ca.length < 2)
				user.schedTell("You need to give me a message to send. Syntax: /me MESSAGE");
			else
				synchronized(users) {
					for (UserConnection u : users)
						if(u.isAccepted())
							u.ircMe(user.getNick(), ca[1]);
				}
		} else if (ca[0].equalsIgnoreCase("/fuckyoubecause")) {
			synchronized(users) {
				for (UserConnection u : users)
					u.schedTell("The admin has been called. Might be away from the keyboard though, in which case tough luck.");
			}
			System.out.println("\n\n\nSomeone fucking called you!\n\n\n");
		} else if (ca[0].equalsIgnoreCase("/nick") || ca[0].equalsIgnoreCase("/name")) {
			if (ca.length < 2) {
				user.schedTell("You need to give me a name.");
			} else {
				ca[1] = ca[1].replace(" ", "-").replace("\n", "").replace("\r", "");
				boolean go = true;
				if (ca[1].toLowerCase().matches("adm[iı]n"))
					go = false;
				synchronized(deduper) {
					if (deduper.contains(ca[1]))
						go = false;
				}
				if (go) {
					tellRoom("'" + user.getNick() + "' is now known as '" + ca[1] + "'");
					user.setNick(ca[1]);
				} else {
					user.schedTell("Someone already has that name. Try another one.");
				}
			}
		} else if (ca[0].equalsIgnoreCase("/cap")) {
			if (ca.length < 2 || !userCaptchas.containsKey(user)) {
				CaptchaDialog cd = captchas.get((int) (Math.random() * captchas.size()));
				synchronized(userCaptchas) {
					userCaptchas.put(user, cd);
				}
				for (String line : cd.getArt().split("\n "))
					user.schedTell(line);
				user.schedTell("Type /cap plus the solution to submit. /cap alone tries to get another captcha.");
			} else {
				CaptchaDialog cd = userCaptchas.get(user);
				try {
					if (cd.send(ca[1])) {
						synchronized(captchas) {
							captchas.remove(cd);
						}
						synchronized(userCaptchas) {
							userCaptchas.remove(user);
						}
						user.schedTell("win");
					} else {
						user.schedTell("fail");
						cd.update();
					}
				} catch (Exception e) {
					user.schedTell("Something went wrong. Try doing a /cap again.");
					e.printStackTrace();
				};
			}
		} else if (ca[0].equalsIgnoreCase("/list") || ca[0].equalsIgnoreCase("/who")) {
			user.schedTell("There are " + welcomed.size() + " users in the room:");
			StringBuilder list = new StringBuilder("");
			synchronized(users) {
				for (UserConnection u : users)
					if (u == user)
						list.append("\n    ").append(u.getNick()).append(" <- you");
					else if (u.isReady())
						list.append("\n    ").append(u.getNick());
			}
			if (list.length() > 1)
				user.schedTell(list.substring(1));
		} else if (ca[0].equalsIgnoreCase("/spam")) {
			int joinRestSec = joinFreq() * 3 / 2000;
			int joinFreqSec = joinFreq() / 1000;
			if (autoJoin) {
				if (welcomed.size() >= maxChatting)
					user.schedTell("Spamming is paused because the room is full (max " + maxChatting + " users). It will resume once someone leaves.");
				else if (users.size() >= maxTotal)
					user.schedTell("Spamming is paused because of lingering connections. It will resume within " + joinRestSec +" seconds.");
				else
					user.schedTell("Spamming is already on. Current rate is " + joinFreqSec + " seconds.");
			} else {
				autoJoin = true;
				tellRoom(user.getNick() + " has turned spamming on.");
				
				if (welcomed.size() >= maxChatting)
					tellRoom("Spamming is paused because the room is full (max " + maxChatting + " users). It will resume once someone leaves.");
				else if (users.size() >= maxTotal)
					tellRoom("Spamming is paused because of lingering connections. It will resume within " + joinRestSec + " seconds.");
			}
		} else if (ca[0].equalsIgnoreCase("/nospam")) {
			if (!autoJoin) {
				user.schedTell("Spamming is already off.");
			} else {
				autoJoin = false;
				tellRoom(user.getNick() + " has turned spamming off.");
			}
		} else if (ca[0].equalsIgnoreCase("/kick")) {
			if (welcomed.size() == 1) {
				user.schedTell("There isn't anyone there to kick.");
			} else if (userFromName(ca[1]) == null) {
				user.schedTell("No user exists with that name. Maybe they left of their own accord?");
			} else {
				tellRoom(user.getNick() + " has voted to kick " + ca[1] + " from the room.");
				voteKick(userFromName(ca[1]), user);
			}
		} else if (ca[0].equalsIgnoreCase("/dontkick") || ca[0].equalsIgnoreCase("/unkick") || ca[0].equalsIgnoreCase("/nokick")) {
			if (welcomed.size() == 1) {
				user.schedTell("There isn't anyone there to kick.");
			} else if (userFromName(ca[1]) == null) {
				user.schedTell("No user exists with that name, maybe they're already gone");
			} else {
				unvoteKick(userFromName(ca[1]), user);
				tellRoom(user.getNick() + " has withdrawn their vote to kick " + ca[1] + " from the room.");
			}
		} else {
			user.schedTell("That isn't a known command. Type /help for more info.");
		}
	}
	
	private String[] youHave = {"You've ", "You have ",};
	private String[] stumbled = {"stumbled upon ", "come upon ", 
		"been chosen to take part in ", "been selected to enter ",};
	private String[] entered = {"stumbled upon ", "come upon ", 
		"entered  ", "found ",};
	private String[] roving = {"a roving ", "a wandering ", "a ", "an Omegle ",};
	private String[] groupChat = {"group chat. ", "groupchat. ", "multi-user chat. ", "chat room. ", "chatroom. ",};
	private String[] doNote = {"Do note, ", "Note that ", "Please be aware that ", "Do be aware that ",};
	private String[] notThePlace = {"this is not the place to ", "this is not a place to ",};
	private String[] flauntGenitalia = {"flaunt the fitness of your sex organs", "brag about your sexual prowess", 
		"advertise your physical attractiveness", "display your undoubtedly amazing genitalia",};
	private String[] orAnything = {" or somesuch. ", " or anything. ", " or anything like that. ", ". ",};
	
	private String[] toothpaste = {"To enter the __room__, you need to say \"toothpaste\". ",
		"Type \"toothpaste\" to enter the __room__.", "Enter the __room__ by typing \"toothpaste\". ",
		"To enter the __room__, say the word \"toothpaste\" as a show of human conformity. ",
		"To enter the __room__, say the word \"toothpaste\". ",};
	private String[] room = {"room", "chat room", "chatroom", "chat",};
	
	private String[] nobody = {"No one else has entered the __room__ yet", "The __room__ is still empty", 
		"Nobody else has joined yet",};
	private String[] so = {", so ", " - ",};
	private String[] boring = {"be prepared for a bore.", "you may need to wait a while.",
		"you'll need to be patient.",};
	
	private String[] youAre = {"You are ", "You happen to be ", "You're ",};
	private String[] firstUser = {"the first one ", "the first user ",};
	private String[] inTheRoom = {"in the __room__. ", "in this __room__. ", "here. ", "to join. ",};
	private String[] holdOn = {"Hold on", "You'll have to wait a while",};
	private String[] lookFor = {", we're looking for ", " while we look for ", ", we're trying to find ",};
	private String[] someoneElse = {"someone else. \n", "another user. \n", "more users. \n", "another stranger. \n",};
	
	private String[] nOthers = {"There are __num__ __others__ in the __room__", 
		"__num__ __others__ are in the __room__",};
	private String[] others = {"users", "others", "other users", "other strangers", "other losers",};
	
	private String randomize(String... choices) {
		return choices[(int) (Math.random() * choices.length)];
	}
	private String randomize(String[]... choices) {
		StringBuilder b = new StringBuilder();
		for (String[] set : choices)
			b.append(randomize(set));
		return b.toString();
	}
	
	private void welcome(UserConnection user) {
		if (user.isAccepted()) {
			synchronized(welcomed) {
				if (welcomed.contains(user))
					return;
			}
			
			boolean isInformed = false;
			synchronized(informed) {
				if (informed.contains(user))
					isInformed = true;
			}
			boolean separateFirstLine = Math.random() > 0.66;
			String firstLine = randomize("This is ", "You're in ", randomize(youHave, entered)) + randomize(roving, groupChat);
			if (!isInformed && separateFirstLine)
				user.schedTell(firstLine);
				
			if (welcomed.size() < 1) {
				user.schedTell(randomize(youAre, firstUser, inTheRoom, holdOn, lookFor, someoneElse)
					.replace("__room__", randomize(room)) +
					"(Your username will be: '" + user.getNick() + "')");
				if (!murder)
					add();
			} else {
				StringBuilder otherUsers = new StringBuilder();
				synchronized(users) {
					for (UserConnection u : users)
						if (u != user && u.isReady()) {
							otherUsers.append(", ").append(u.getNick());
							if (user.getQuestion() != null)
								u.schedTell(user.getNick() + " has joined with question: '" + user.getQuestion() + "'");
							else
								u.schedTell(user.getNick() + " has joined");
						}
				}
				boolean showUsers = otherUsers.length() > 2 && Math.random() > 1d/3;
				String countLine = randomize(nOthers).replace("__room__", randomize(room)).replace("__others__",
					randomize(others)).replace("__num__", String.valueOf(welcomed.size())) + (showUsers ? ": " : ".");
				if (!separateFirstLine && !isInformed && Math.random() > 0.5)
					user.schedTell(firstLine + countLine);
				else
					user.schedTell(countLine);
				if (showUsers)
					if (Math.random() > 0.5)
						user.schedTell("    " + otherUsers.substring(2));
					else
						for (String otherUser : otherUsers.substring(2).split(", "))
							user.schedTell("    " + otherUser);
				user.schedTell("'" + user.getNick() + "' is your username. Everyone sees it in front of the messages you send.");
			}
			user.schedTell("Type '/nick NAME' to change your name, or '/help' to see the commands you can use.");
			synchronized(welcomed) {
				welcomed.add(user);
			}
		} else {
			if (informed.contains(user))
				return;
			UserConnection.sleep((int) (Math.random() * 700));
			
			String stumbledLine = randomize(youHave, stumbled, roving, groupChat);
			String flauntLine = randomize(doNote, notThePlace, flauntGenitalia, orAnything);
			String pasteLine = randomize(toothpaste).replace("__room__", randomize(room));
			
			if (Math.random() < 0.5) {
				if (Math.random() < 0.5) {
					user.schedTell(stumbledLine);
					user.schedTell(flauntLine);
					user.schedTell(pasteLine);
				} else {
					user.schedTell(stumbledLine + flauntLine);
					user.schedTell(pasteLine);
				}
			} else {
				if (Math.random() < 0.5) {
					user.schedTell(stumbledLine);
					user.schedTell(flauntLine + pasteLine);
				} else {
					user.schedTell(stumbledLine);
					user.schedTell(pasteLine + flauntLine);
				}
			}
				
			if (welcomed.size() < 2)
				user.schedTell("(" + randomize(nobody, so, boring).replace("__room__", randomize(room)) + ")");
			synchronized(informed) {
				informed.add(user);
			}
		}
			
	}
	
	private void kickVoted(UserConnection u) {
		kick(u, "(The room voted to kick this user)");
	}
	private void kickInactive(UserConnection u) {
		kick(u, "(The user was inactive for too long)");
	}
	private void kickBot(UserConnection u) {
		kick(u, "(Their first messages looked like spam)");
	}
	
	private void kick(UserConnection u, String message) {
		if (u == null) {
			System.err.println("not a user (tried to kick: " + message + ")");
		} else {
			u.sendDisconnect();
			u.dispose();
			remove(u);
			System.out.println(u.getNick() + " has been kicked: " + message);
			if (u.didLive())
				tellRoom(message);
		}
	}
	
	private void kick(String name) {
		UserConnection u = userFromName(name);
		if (u == null) {
			System.err.println(name + " isn't a user");
		} else {
			kick(u, "(The user was kicked by the admin)");
		}
	}
	
	
	public void callback(UserConnection user, String method, String data) {
		//System.out.println("callback: '" + user + "', '" + method + "', '" + data + "'");
		if (method.equals("captcha")) {
			autoJoin = false;
			System.out.println("\n\nCaptcha received - spamming turned off\n\n");
			System.out.println("(Freqs until now: spam " + joinFreq + " pulse " + pulseFreq);
			pulseFreq = -1;
			murder = true;
			if (captchasPublic)
				tellRoom("We've been captcha'd! Spamming is now off. Type /cap to try to solve one.");
			else
				tellRoom("We've been captcha'd! No new users can join, unless the admin manually solves captchas. All you can do is watch the room die.");
						
			synchronized(captchas) {
				captchas.add(new CaptchaDialog(data, user.getID()));
			}
		}
		else if (method.equals("ban"))
			banned();
		else if (method.equals("died"))
			remove(user);
		else if (method.equals("disconnected"))
			remove(user);
		else if (method.equals("typing"))
			typing(user);
		else if (method.equals("message"))
			hear(user, data);
		else if (method.equals("connected") || method.equals("accepted"))
			welcome(user);
		else if (method.equals("pulse success"))
			addPulse();
		else
			System.err.println("\n\n\nUNKNOWN CALLBACK METHOD '" + method + "'\n\n\n");
	}
	
	private static Multi m;
	private static UserConnection consoleDummy;
	
	public static void main(String[] args) {
		m = new Multi();
		consoleDummy = new UserConnection(m) {
			public void sendAction(String s, String t) {
				System.out.println("[Dummy-" + s + "]" + t);
			}
			public void schedTell(String str) {
				tell(str);
			}
			public void schedTell(String one, String two) {
				tell(one, two);
			}
			{ setNick("Admin"); dummy(); }
		};
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		loadRc(m, null);
		while (true) {
			System.err.print("> ");
			try {
				String cmd = in.readLine();
				commandLine(m, cmd);
			} catch (Exception e) { e.printStackTrace(); }
		}
	}
	
	static void loadRc(Multi m, String name) {
		if (name == null)
			name = ".multirc";
		try {
			BufferedReader file = new BufferedReader(new FileReader(name));
			String cmd = null;
			while ((cmd = file.readLine()) != null)
				commandLine(m, cmd);
		} catch (FileNotFoundException e) {
			// nop
		} catch (Exception e) {
			System.err.println("Could not load the rc file. The exception follows.");
			e.printStackTrace();
		}
	}
																																	
	
	public static void commandLine(Multi m, String c) {
		if (c.equals("")) {
			// this is a nop. a what? a nop. a what? a nop. oh, a nop.
		} else if (c.startsWith("broad")) {
			m.hear(consoleDummy, c.substring(6));
		} else if (c.startsWith("unichar")) {
			System.out.println(UserConnection.unicodify(c.substring(8)));
		} else if (c.startsWith("kick")) {
			m.kick(c.substring(5));
		} else if (c.equals("invite")) {
			m.add();
		} else if (c.startsWith("qfreq")) {
			double f = Double.valueOf(c.substring(6));
			if (f >= 0 && f <= 1)
				m.qfreq = f;
			else
				System.err.println("Must be between 0 and 1");
		} else if (c.equals("inviteq")) {
			m.add(true);
		} else if (c.equals("invitec")) {
			m.add(null);
		} else if (c.equals("invitep")) {
			m.addPulse();
		} else if (c.equals("list")) {
			for (UserConnection u : m.users)
				synchronized(m.welcomed) {
					System.out.println(" * " + u.getNick() + (u.isPulse() ? " (pulse)" : 
						u.isAccepted() ? m.welcomed.contains(u) ? "" : " (dead/zombie)" : " (untriaged)"));
				}
		} else if (c.startsWith("freq")) {
			int f = Integer.valueOf(c.substring(5));
			if (f >= 250)
				m.joinFreq = Integer.valueOf(c.substring(5));
			else
				System.err.println("Must be 250 or greater (milliseconds, not seconds)");
		} else if (c.startsWith("pfreq")) {
			int f = Integer.valueOf(c.substring(6));
			m.pulseFreq = f;
		} else if (c.equals("verbose")) {
			UserConnection.verbose = true;
		} else if (c.equals("terse")) {
			UserConnection.verbose = false;
		} else if (c.equals("murder")) {
			m.murder = true;
		} else if (c.equals("revive")) {
			m.murder = false;
		} else if (c.equals("beg")) {
			m.captchasPublic = true;
		} else if (c.equals("serve")) {
			m.captchasPublic = false;
		} else if (c.startsWith("ban ")) {
			m.ban(c.substring(4));
		} else if (c.equals("bans")) {
			for (String ban : m.getBans()) {
				System.out.println("ban " + ban);
			}
		} else if (c.equals("clearbans")) {
			m.clearBans();
		} else if (c.startsWith("loadrc")) {
			loadRc(m, c.length() > 6 ? c.substring(7) : null);
		} else if (c.startsWith("/")) {
			m.command(consoleDummy, c);
		} else {
			System.err.println("That isn't a command.\n" +
				"Valid commands are the slash commands (type /help for info), plus these which don't have slashes:\n" +
				" broad MESSAGE      Broadcast a message\n" +
				" kick USER          Kick the user immediately\n" +
				" invite             Invite a user, " + m.qfreq + " from question mode\n" +
				" qfreq FREQ         Set the question mode fraction to FREQ\n" +
				" inviteq            Invite a user from question mode\n" +
				" invitec            Invite a user from chat mode\n" +
				" invitep            Invite a returning user (\"pulse\") from chat mode\n" +
				" list               List all users, including untriaged\n" +
				" freq FREQ          Set the spam frequency to FREQ (currently " + m.joinFreq + ") milliseconds\n" +
				" pfreq FREQ         Set the pulse frequency to FREQ (currently " + m.pulseFreq + ") milliseconds\n" +
				"   (Limited by spam frequency. -1 to disable pulse)\n" +
				" verbose            Show messages sent to users by the system\n" +
				" terse              Hide verbose messages (default)\n" +
				"   (Currently " + (UserConnection.verbose ? "verbose" : "terse") + ")\n" +
				" murder             Allow room to die out if spamming is off\n" +
				" revive             Try to revive room when last user leaves (default)\n" +
				"   (Currently " + (m.murder ? "murder-happy" : "revive-happy") + ")\n" +
				" beg                Notify users of captchas to solve\n" +
				" serve              Don't beg (default)\n" +
				"   (Currently " + (m.captchasPublic ? "ready to beg" : "ready to serve") + ")\n" +
				" ban REGEXP         Automatically kick users opening (first 3 lines) with text matching REGEXP\n" +
				" bans               List bans\n" +
				" clearbans          Remove all bans\n" +
				" loadrc FILE        Run console commands from the FILE. Beware of recursion!\n");
		}
	}
	
}
