package anon.trollegle;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URLDecoder;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class Trollegle extends JFrame implements ActionListener, Callback
{
	public static boolean fillSayings = false;
	private static boolean isQM;
	public static final ArrayList<String> sayings = new ArrayList<String>(Arrays.asList(new String[]{
		"mm",
		"yeah...",
		"true",
		"yeah",
		"heh, yeah",
		"good point",
		"maybe...",
		"I'm bored",
		"¡This cheese is burning me!",
		"you bet!",
		"asl?",
		"pick a number between 1 and 100",
		"...?",
		"wanna cyber?",
		"Okay, that's fine by me!",
		"and I don't know it's just, I don't know I just think it's like, everything's like 'buy now' these days you know?",
		"I can't just now... sorry...",
		"WHAT IS LOVE",
		"I'm afraid not",
		"no",
		"Well what do you THINK?",
		"Exception in thread \"Thread-35\" java.lang.NullPointerException",
		"Exception in thread \"Thread-0\" java.io.FileNotFoundException",
	}));
	public static final ArrayList<String[]> clbuttisms = new ArrayList<String[]>(Arrays.asList(new String[][]{
		{"(?i)a+ss+wi+p+e+", "BIG PENISES. PENIS PENIS PENIS PENIS!"},
		{"(?i)a+ss+", "butt"},
		{"(?i)co+ck+", "dick"},
		{"(?i)ti+t+y?(ies)?", "breast"},
		{"(?i)cu+m", "seed"},
		{"(?i)cu+nt+", "vulva"},
		{"(?i)pus+sy+", "vulva"},
		{"(?i)ga+y+", "homosexual"},
		{"(?i)s+h+i+t+", "poop"},
		{"(?i)s+e+(x+|ck+s+)", "love"},
		{"(?i)blo+w+ ?jo+b", "linguistics"},
		{"(?i)fa+g+o+t+", "bassoon"},
	}));
	public static interface Bot { String answer(String msg); }
	public static final HashMap<String, Bot> bots = new HashMap<>(); static {
		bots.put("thingstosay", new Bot() {
			public String toString() { return "thingstosay"; };
			public String answer(String msg) {
				return Trollegle.sayings.get((int)(Math.random() * Trollegle.sayings.size()));
			}
		});
		bots.put("count", new Bot() {
			long you;
			public String toString() { return "count"; };
			public String answer(String msg) {
				return " " + ++you;
			}
		});
		bots.put("pete", new Bot() {
			ArrayDeque<String> stream = new ArrayDeque<>(); {
				stream.add("hi");
				stream.add("why?");
				stream.add("i like trees");/*
				stream.add("lel 420 blaze it fgt omggggggggggg");
				stream.add("no im not stpuidd");
				stream.add("do u have nice duck");
				stream.add("twss");
				stream.add("if you put two third tones in a row it becomes a second tone");*/
			}
			public String toString() { return "pete"; };
			public String answer(String msg) {
				if (msg.matches("(?i)(^m$|^hi+[^a-z]*$|^he+y+[^a-z]*$|" + 
						"^[fm]( or |/| )[fm][^a-z]*$|^asl[^a-z]$)"))
					msg = bots.get("thingstosay").answer("");
				else if (msg.matches("(?i)(yo?)?ur name[^a-z]*$"))
					return UserConnection.names[(int) (Math.random() * UserConnection.names.length)] + " you?";
				stream.add(msg);
				return stream.poll();
			}
		});
	}
	public static String bot = "thingstosay";
	int n = 0;
    static UserConnection aConn;
    static UserConnection bConn;
    private static JTextArea leftArea = new JTextArea();
    private static JTextArea rightArea = new JTextArea();
    static JTextField leftInputField;
    static JTextField rightInputField;
    JScrollPane leftScroll;
    JScrollPane rightScroll;
    JCheckBox leftInvertBox;
    JCheckBox leftAppendBox;
    JTextField leftAppendField;
    JCheckBox leftReplaceBox;
    JTextField leftOriginalField;
    JTextField leftDestinationField;
	JCheckBox leftClbuttifyBox;
    JCheckBox rightInvertBox;
    JCheckBox rightAppendBox;
    JTextField rightAppendField;
    JCheckBox rightReplaceBox;
    JTextField rightOriginalField;
    JTextField rightDestinationField;
	JCheckBox rightClbuttifyBox;
    JLabel leftTyping;
    JLabel rightTyping;
	JCheckBox leftTroll;
    private static Action sl = new AbstractAction("sl") {
	public void actionPerformed(ActionEvent e) {
		try { Thread.sleep(200); } catch (Exception ex) { ex.printStackTrace(); }
	    Trollegle.bConn.sendMessage(Trollegle.leftInputField.getText());
	    Trollegle.leftArea.setText(new StringBuilder
					   (Trollegle.leftArea.getText())
					   .append
					   ("\n").append
					   ("You: ").append
					   (Trollegle.leftInputField.getText())
					   .toString());
	    Trollegle.rightArea.setText(new StringBuilder
					    (Trollegle.rightArea.getText())
					    .append
					    ("\n").append
					    ("Stranger: ").append
					    (Trollegle.leftInputField
						 .getText())
					    .toString());
	    Trollegle.leftInputField.setText("");
	    Trollegle.leftArea.selectAll();
	    int x = Trollegle.leftArea.getSelectionEnd();
	    Trollegle.leftArea.select(x, x);
	    Trollegle.rightArea.selectAll();
	    x = Trollegle.rightArea.getSelectionEnd();
	    Trollegle.rightArea.select(x, x);
	}
    };
    private static Action sr = new AbstractAction("sr") {
	public void actionPerformed(ActionEvent e) {
		try { Thread.sleep(200); } catch (Exception ex) { ex.printStackTrace(); }
	    Trollegle.aConn.sendMessage(Trollegle.rightInputField.getText());
	    Trollegle.rightArea.setText(new StringBuilder
					    (Trollegle.rightArea.getText())
					    .append
					    ("\n").append
					    ("You: ").append
					    (Trollegle.rightInputField
						 .getText())
					    .toString());
	    Trollegle.leftArea.setText(new StringBuilder
					   (Trollegle.leftArea.getText())
					   .append
					   ("\n").append
					   ("Stranger: ").append
					   (Trollegle.rightInputField
						.getText())
					   .toString());
	    Trollegle.rightInputField.setText("");
	    Trollegle.leftArea.selectAll();
	    int x = Trollegle.leftArea.getSelectionEnd();
	    Trollegle.leftArea.select(x, x);
	    Trollegle.rightArea.selectAll();
	    x = Trollegle.rightArea.getSelectionEnd();
	    Trollegle.rightArea.select(x, x);
	}
    };
    
    public void actionPerformed(ActionEvent e) {
	if (e.getActionCommand().equals("Disconnect left guy")) {
	    if(aConn.isAlive()) aConn.sendDisconnect();
		aConn.dispose();
	    leftArea.setText(new StringBuilder(leftArea.getText()).append
				 ("\n").append
				 ("This guy has been disconnected.")
				 .toString());
	    leftTyping.setText(" ");
	} else if (e.getActionCommand().equals("Get new left guy")) {
	    if(aConn.isAlive()) aConn.sendDisconnect();
		aConn.dispose();
	    leftArea.setText(new StringBuilder(leftArea.getText()).append
				 ("\n").append
				 ("This guy has been disconnected.")
				 .toString());
	    aConn = new UserConnection(this, true, isQM);
	    aConn.start();
	    leftTyping.setText(" ");
	} else if (e.getActionCommand().equals("Get two new guys")) {
	    if(aConn.isAlive()) aConn.sendDisconnect();
	    if(bConn.isAlive()) bConn.sendDisconnect();
		aConn.dispose();
		bConn.dispose();
	    leftArea.setText(new StringBuilder(leftArea.getText()).append
				 ("\n").append
				 ("This guy has been disconnected.")
				 .toString());
	    rightArea.setText(new StringBuilder(rightArea.getText()).append
				  ("\n").append
				  ("This guy has been disconnected.")
				  .toString());
	    aConn = new UserConnection(this, true, isQM);
	    bConn = new UserConnection(this, true, isQM);
	    aConn.start();
	    bConn.start();
	    leftTyping.setText(" ");
	    rightTyping.setText(" ");
	} else if (e.getActionCommand().equals("Disconnect right guy")) {
	    if(bConn.isAlive()) bConn.sendDisconnect();
		bConn.dispose();
	    rightArea.setText(new StringBuilder(rightArea.getText()).append
				  ("\n").append
				  ("This guy has been disconnected.")
				  .toString());
	    rightTyping.setText(" ");
	} else if (e.getActionCommand().equals("Get new right guy")) {
	    if(bConn.isAlive()) bConn.sendDisconnect();
		bConn.dispose();
	    rightArea.setText(new StringBuilder(rightArea.getText()).append
				  ("\n").append
				  ("This guy has been disconnected.")
				  .toString());
	    bConn = new UserConnection(this, true, isQM);
	    bConn.start();
	    rightTyping.setText(" ");
	}
	leftArea.selectAll();
	int x = leftArea.getSelectionEnd();
	leftArea.select(x, x);
	rightArea.selectAll();
	x = rightArea.getSelectionEnd();
	rightArea.select(x, x);
    }
    
    public Trollegle() {
	super("Trollegle");
	JPanel pnl = new JPanel();
	setSize(1024, 768);
	setDefaultCloseOperation(3);
	setVisible(true);
	setResizable(true);
	GridLayout b = new GridLayout();
	pnl.setLayout(b);
	setLayout(new BorderLayout());
	add(pnl, "Center");
	JPanel leftPanel = new JPanel();
	JPanel rightPanel = new JPanel();
	leftArea.setLineWrap(true);
	rightArea.setLineWrap(true);
	leftScroll = new JScrollPane(leftArea, 20, 31);
	rightScroll = new JScrollPane(rightArea, 20, 31);
	leftPanel.setLayout(new BorderLayout());
	rightPanel.setLayout(new BorderLayout());
	JPanel leftInnerPanel = new JPanel();
	leftInnerPanel.setLayout(new BorderLayout());
	JPanel rightInnerPanel = new JPanel();
	rightInnerPanel.setLayout(new BorderLayout());
	leftTyping = new JLabel("This guy is typing.");
	leftTyping.setFont(new Font("Arial", 1, 12));
	leftTyping.setAlignmentX(0.5F);
	leftTyping.setText(" ");
	rightTyping = new JLabel("This guy is typing.");
	rightTyping.setFont(new Font("Arial", 1, 12));
	rightTyping.setAlignmentX(0.5F);
	rightTyping.setText(" ");
	leftInnerPanel.add(leftTyping, "South");
	rightInnerPanel.add(rightTyping, "South");
	leftInnerPanel.add(leftScroll, "Center");
	rightInnerPanel.add(rightScroll, "Center");
	leftPanel.add(leftInnerPanel, "Center");
	rightPanel.add(rightInnerPanel, "Center");
	pnl.add(leftPanel);
	pnl.add(rightPanel);
	JPanel leftBottomPanel = new JPanel();
	leftBottomPanel.setLayout(new BorderLayout());
	leftInputField = new JTextField();
	leftBottomPanel.add(leftInputField, "Center");
	JButton leftSendButton = new JButton("Send");
	leftPanel.add(leftBottomPanel, "South");
	leftInputField.setAction(sl);
	leftInputField.getInputMap().put(KeyStroke.getKeyStroke("ENTER"),
					 "SL");
	leftInputField.getActionMap().put("SL", sl);
	JPanel rightBottomPanel = new JPanel();
	rightBottomPanel.setLayout(new BorderLayout());
	rightInputField = new JTextField();
	rightBottomPanel.add(rightInputField, "Center");
	JButton rightSendButton = new JButton("Send");
	rightPanel.add(rightBottomPanel, "South");
	rightInputField.setAction(sr);
	rightInputField.getInputMap().put(KeyStroke.getKeyStroke("ENTER"),
					  "SR");
	rightInputField.getActionMap().put("SR", sr);
	JPanel leftTopPanel = new JPanel();
	leftInvertBox = new JCheckBox();
	leftInvertBox.setText("Flip");
	leftTopPanel.add(leftInvertBox);
	leftAppendBox = new JCheckBox();
	leftAppendBox.setText("Append:");
	leftTopPanel.add(leftAppendBox);
	leftAppendField = new JTextField();
	leftAppendField.setText("appendage");
	leftTopPanel.add(leftAppendField);
	leftReplaceBox = new JCheckBox();
	leftReplaceBox.setText("Replace: ");
	leftOriginalField = new JTextField("asl");
	leftDestinationField = new JTextField("replacement");
	leftClbuttifyBox = new JCheckBox();
	leftClbuttifyBox.setText("Clbuttify");
	leftTopPanel.add(leftReplaceBox);
	leftTopPanel.add(leftOriginalField);
	leftTopPanel.add(leftDestinationField);
	leftTopPanel.add(leftClbuttifyBox);
	leftPanel.add(leftTopPanel, "North");
	JPanel rightTopPanel = new JPanel();
	rightInvertBox = new JCheckBox();
	rightInvertBox.setText("Flip");
	rightTopPanel.add(rightInvertBox);
	rightAppendBox = new JCheckBox();
	rightAppendBox.setText("Append:");
	rightTopPanel.add(rightAppendBox);
	rightAppendField = new JTextField();
	rightAppendField.setText("appendage");
	rightTopPanel.add(rightAppendField);
	rightReplaceBox = new JCheckBox();
	rightReplaceBox.setText("Replace: ");
	rightOriginalField = new JTextField("asl");
	rightDestinationField = new JTextField("replacement");
	rightClbuttifyBox = new JCheckBox();
	rightClbuttifyBox.setText("Clbuttify");
	rightTopPanel.add(rightReplaceBox);
	rightTopPanel.add(rightOriginalField);
	rightTopPanel.add(rightDestinationField);
	rightTopPanel.add(rightClbuttifyBox);
	rightPanel.add(rightTopPanel, "North");
	JPanel bottomPanel = new JPanel();
	bottomPanel.setLayout(new GridLayout());
	JButton disconnectLeft = new JButton("Disconnect left guy");
	disconnectLeft.addActionListener(this);
	bottomPanel.add(disconnectLeft);
	JButton fetchNewLeft = new JButton("Get new left guy");
	fetchNewLeft.addActionListener(this);
	bottomPanel.add(fetchNewLeft);
	JButton getTwoNew = new JButton("Get two new guys");
	getTwoNew.addActionListener(this);
	bottomPanel.add(getTwoNew);
	JButton disconnectRight = new JButton("Disconnect right guy");
	disconnectRight.addActionListener(this);
	bottomPanel.add(disconnectRight);
	JButton fetchNewRight = new JButton("Get new right guy");
	fetchNewRight.addActionListener(this);
	bottomPanel.add(fetchNewRight);
	leftTroll = new JCheckBox("Troll left");
	bottomPanel.add(leftTroll);
	add(bottomPanel, "South");
	leftArea.setText("Trollegle has been started.");
	rightArea.setText("Trollegle has been started.");
	work();
    }
    
    public void callback(UserConnection user, String action, String parameter) {
	UserConnection otherGuy = null;
	UserConnection thisGuy = null;
	System.out.print("user=="); System.out.println(user);
	System.out.print("action=="); System.out.println(action);
	System.out.print("parameter=="); System.out.println(parameter);
	if (user == aConn) {
	    otherGuy = bConn;
	    thisGuy = aConn;
	} else {
	    thisGuy = bConn;
	    otherGuy = aConn;
	}
	if (action.equals("captcha")) {
		new CaptchaDialog(parameter, thisGuy.getID());
	} else if (action.equals("disconnected")) {
	    if (thisGuy == aConn)
		leftArea.setText(new StringBuilder(leftArea.getText()).append
				     ("\n").append
				     ("You have disconnected.").toString());
	    else
		rightArea.setText(new StringBuilder(rightArea.getText()).append
				      ("\n").append
				      ("You have disconnected.").toString());
	    if (thisGuy == aConn)
		leftTyping.setText(" ");
	    else
		rightTyping.setText(" ");
	} else if (action.equals("typing")) {
	    otherGuy.sendTyping();
	    if (thisGuy == aConn)
		leftTyping.setText("This guy is typing...");
	    else
		rightTyping.setText("This guy is typing...");
	} else if (action.equals("message")) {
	    String msg = URLDecoder.decode(parameter);
		if (fillSayings)
			sayings.add(msg);
	    String oMsg = msg;
	    String msg2 = "";
	    for (int i = 0; i < msg.length(); i++)
		msg2 = new StringBuilder(msg2).append
			   (msg.charAt(msg.length() - 1 - i)).toString();
	    if (thisGuy == aConn) {
			if (leftReplaceBox.isSelected())
				msg = msg.replaceAll(leftOriginalField.getText(),
						  leftDestinationField.getText());
			if (leftInvertBox.isSelected())
				msg = msg2;
			if (leftAppendBox.isSelected())
				msg = new StringBuilder(msg).append(" ").append
					  (leftAppendField.getText()).toString();
			if (leftClbuttifyBox.isSelected())
				for(String[] clbuttism : clbuttisms)
					msg = msg.replaceAll(clbuttism[0], clbuttism[1]);
	    } else {
			if (rightReplaceBox.isSelected())
				msg = msg.replaceAll(rightOriginalField.getText(),
						  rightDestinationField.getText());
			if (rightInvertBox.isSelected())
				msg = msg2;
			if (rightAppendBox.isSelected())
				msg = new StringBuilder(msg).append(" ").append
					  (rightAppendField.getText()).toString();
			if (rightClbuttifyBox.isSelected())
				for(String[] clbuttism : clbuttisms)
					msg = msg.replaceAll(clbuttism[0], clbuttism[1]);
	    }
	    otherGuy.sendMessage(msg);
	    if (thisGuy == aConn) {
		leftTyping.setText(" ");
		leftArea.setText(new StringBuilder(leftArea.getText()).append
				     ("\n").append
				     ("You: ").append
				     (oMsg).toString());
		rightArea.setText(new StringBuilder(rightArea.getText()).append
				      ("\n").append
				      ("Stranger: ").append
				      (msg).toString());
	    } else {
		rightTyping.setText(" ");
		rightArea.setText(new StringBuilder(rightArea.getText()).append
				      ("\n").append
				      ("You: ").append
				      (oMsg).toString());
		leftArea.setText(new StringBuilder(leftArea.getText()).append
				     ("\n").append
				     ("Stranger: ").append
				     (msg).toString());
	    }
		if (thisGuy == aConn && leftTroll.isSelected()) {
			thisGuy.sendTyping();
			try {
				Thread.sleep(1500);
			} catch(Exception e) { e.printStackTrace(); }
			String thing = Trollegle.bots.get(Trollegle.bot).answer(oMsg);
			thisGuy.sendMessage(thing);
			leftArea.setText(leftArea.getText() + "\nStranger: " + thing);
			if(msg.toLowerCase().indexOf("bot") != -1) {
				try {
					Thread.sleep(600);
				} catch(Exception e) { e.printStackTrace(); }
				thing = "You're the " + ++n + "th person to call me a bot today, and you're right. ";
				thisGuy.sendMessage(thing);
				leftArea.setText(leftArea.getText() + "\nStranger: " + thing);
				try {
					Thread.sleep(600);
				} catch(Exception e) { e.printStackTrace(); }
				//thing = "See: http://forums.xkcd.com/viewtopic.php?p=1900381&sid=b8e3aa84f5e53428793d081f6d87db27#p1900381";
				thing = "See: http://forums.xkcd.com/viewtopic.php?p=1900381";
				thisGuy.sendMessage(thing);
				leftArea.setText(leftArea.getText() + "\nStranger: " + thing);
			}
		}
	} else if (action.equals("died")) {
	    System.out.println(user +
		     ": The connection dropped due to this exception: " + parameter);
	    work();
	} else if (action.equals("connected")) {
	    if (thisGuy == aConn)
		leftArea.setText
		    (new StringBuilder(leftArea.getText()).append("\n").append
			 ("You're now chatting with a random stranger. Say hi!")
			 .toString());
	    else
		rightArea.setText
		    (new StringBuilder(rightArea.getText()).append("\n").append
			 ("You're now chatting with a random stranger. Say hi!")
			 .toString());
	}
	leftArea.selectAll();
	int x = leftArea.getSelectionEnd();
	leftArea.select(x, x);
	rightArea.selectAll();
	x = rightArea.getSelectionEnd();
	rightArea.select(x, x);
    }
    
    private void work() {
	aConn = new UserConnection(this, true, isQM);
	bConn = new UserConnection(this, true, isQM);
	//aConn.start();
	//bConn.start();
    }
    
    public static void main(String[] args) throws java.io.IOException {
	Trollegle t = new Trollegle();
	BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	try {
		BufferedReader file = new BufferedReader(new FileReader(".trolleglerc"));
		String cmd = null;
		commandLine("!c");
		commandLine("!s");
		while ((cmd = file.readLine()) != null)
			commandLine(cmd);
	} catch (Exception e) {
		System.err.println("Could not load the rc file. This is completely okay. The exception follows.");
		e.printStackTrace();
	}
	while (true) {
		System.err.print("> ");
		String cmd = "";
		try {
			cmd = in.readLine();
		} catch (Exception e) { e.printStackTrace(); }
		commandLine(cmd);
	}
    }
	
	static void commandLine(String cmd) {
		char cmd1 = cmd.length() > 0 ? cmd.charAt(0) : ',';
		char cmd2 = cmd.length() > 1 ? cmd.charAt(1) : ',';
		String data = cmd.length() > 2 ? cmd.substring(2) : "\\|\\|";
		if (cmd1 == 'l') {
			System.out.println("Sayings:");
			for(int i = 0; i < sayings.size(); i++)
				System.out.println(i + ": " + sayings.get(i));
			System.out.println("Clbuttisms:");
			for(int i = 0; i < clbuttisms.size(); i++)
				System.out.println(i + ": " + Arrays.toString(clbuttisms.get(i)));
		} else if (cmd1 == 'c') {
			for(int i = 0; i < sayings.size(); i++)
				System.out.println("as" + sayings.get(i));
			for(int i = 0; i < clbuttisms.size(); i++)
				System.out.println("ac" + clbuttisms.get(i)[0] + "||" + clbuttisms.get(i)[1]);
		} else if (cmd1 == 's') {
			try {
				FileWriter file = new FileWriter(".trolleglerc");
				for(int i = 0; i < sayings.size(); i++)
					file.write("as" + sayings.get(i) + "\n");
				for(int i = 0; i < clbuttisms.size(); i++)
					file.write("ac" + clbuttisms.get(i)[0] + "||" + clbuttisms.get(i)[1] + "\n");
				file.close();
			} catch (Exception e) { e.printStackTrace(); }
		} else if (cmd1 == 'h') {
			System.err.println("l: List");
			System.err.println("c: List in command format");
			System.err.println("s: Save to ./.trolleglerc (loaded on startup)");
			System.err.println("asSAYING: Add saying");
			System.err.println("acREGEX||REPLACEMENT: Add clbuttism");
			System.err.println("rsNUMBER: Remove saying by number");
			System.err.println("rcNUMBER: Remove clbuttism, ditto");
			System.err.println("!s: Remove ALL sayings");
			System.err.println("!c: Remove ALL clbuttisms");
			System.err.println("m: Automatically add everything the strangers say as sayings");
			System.err.println("M: Turn 'm' off (currently " + fillSayings + ")");
			System.err.println("b BOT: set troll bot to BOT (currently " + bot + " of " + bots + ")");
			System.err.println("q: toggle question mode (currently " + isQM + ")");
		} else if (cmd1 == '!') {
			(cmd2 == 'c' ? clbuttisms : sayings).clear();
		} else if (cmd1 == 'a') {
			if (cmd2 == 'c') {
				String[] bits = data.split("\\|\\|");
				if (bits.length == 2)
					clbuttisms.add(new String[] {
						bits[0], bits[1] });
				else
					System.err.println("Invalid format. Must be a regex followed by two pipes then the replacement text.");
			} else {
				sayings.add(data);
			}
		} else if (cmd1 == 'r') {
			ArrayList list = cmd2 == 'c' ? clbuttisms : sayings;
			try {
				list.remove(Integer.parseInt(data));
			} catch (Exception e) { e.printStackTrace(); }
		} else if (cmd1 == 'm')
			fillSayings = true;
		else if (cmd1 == 'M')
			fillSayings = false;
		else if (cmd1 == 'q')
			isQM = !isQM;
		else if (cmd1 == 'b')
			if (bots.containsKey(data))
				bot = data;
			else
				System.err.println(data + " is not a bot in " + bots);
				
		else if (cmd1 == ',') {
			// do nothing
		} else {
			System.err.println("No such command. Valid commands: l c s as ac rs rc !s !c m M b q. Type h for more info.");
		}
	}
}

