package anon.trollegle;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.*;
import java.net.*;

public class CaptchaDialog extends JFrame {
	String site, challenge, server = "http://www.google.com/recaptcha/api/", id;
	boolean incorrect;
	JLabel image;
	String art;
	
	public CaptchaDialog(String site, String id) {
		super("Captcha");
		this.site = site;
		this.id = id;
		setLayout(new BorderLayout());
		JPanel p = new JPanel();
		add(p, "Center");
		GridBagLayout g = new GridBagLayout();
		p.setLayout(g);
		p.add(image = new JLabel("image"));
		g.setConstraints(image, new GridBagConstraints() {{ gridwidth = REMAINDER; }});
		try {
			while(!update());
		} catch (Exception e) { e.printStackTrace(); }
		final JTextField t = new JTextField("");
		p.add(t);
		g.setConstraints(t, new GridBagConstraints() {{ fill = BOTH; weightx = 2d; }});
		t.grabFocus();
		JButton b = new JButton("Submit");
		p.add(b);
		getRootPane().setDefaultButton(b);
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if(send(t.getText()))
						setVisible(false);
					else {
						while(!update()); // FIXME: Request new |site|, somehow
						t.setText("");
					}
				} catch (Exception e1) { e1.printStackTrace(); }
			}
		});
		setVisible(true);
	}
	boolean update() throws Exception {
		//System.out.println("update invoked");
		challenge = null;
		URL u = new URL(server + "challenge?k=" + site + "&ajax=1&cachestop=" + Math.random());
		System.out.println(u);
		URLConnection c = u.openConnection();
		BufferedReader r = new BufferedReader(new InputStreamReader(c.getInputStream()));
		{ String line; while((line = r.readLine()) != null) {
			////System.out.println(line);
			if(line.contains("site : "))
				site = line.split("'")[1];
			else if(line.contains("challenge : "))
				challenge = line.split("'")[1];
			else if(line.contains("is_incorrect : "))
				incorrect = line.contains("true");
			else if(line.contains("server : "))
				server = line.split("'")[1];
		}}
		r.close();
		if(challenge != null) {
			image.setText("<html><img src=\"" + server + "image?c=" + challenge + "\"></img></html>");
			art = printAsskey(server + "image?c=" + challenge);
			//System.out.println("challenge: " + challenge);
			return true;
		}
		return false;
	}
	static String printAsskey(String url) throws Exception {
		BufferedImage image = ImageIO.read(new URL(url));
		StringBuilder sb = new StringBuilder();
		StringBuilder sbr = new StringBuilder();
		for (int y = 0; y < image.getHeight(); y += 2) {
			for (int x = 0; x < image.getWidth() / 2; x += 2) {
				int x2 = x + image.getWidth() / 2;
				sb.append(block(image.getRGB(x, y), image.getRGB(x + 1, y)));
				sbr.append(block(image.getRGB(x2, y), image.getRGB(x2 + 1, y)));
			}
			if (sb.toString().trim().isEmpty()) continue;
			sb.append("\n");
			sbr.append("\n");
		}
		return "Captcha: \n " + sb + " " + sbr;
	}
	private static int BLACK = -16777216, GREY = -1048576;
	static char block(int left, int right) {
		if (left == BLACK)
			if (right == BLACK || right < GREY)
				return '█';
			else
				return '▌';
		else if (right == BLACK)
			if (left < GREY)
				return '█';
			else
				return '▐';
		else if (left < GREY && right < GREY)
			return '▓';
		else if (left < GREY || right < GREY)
			return '▒';
		else
			return '░';
			
	}
	/** Unused! Was only useful while one word was obviously ignored by the captcha algo */
	static CharSequence can(CharSequence... thing) {
		for (CharSequence a : thing) {
			String[] lines = a.toString().split("\n");
			StringBuilder b = new StringBuilder();
			for (String line : lines) {
				int lcount = 0;
				for (int h = 0; h < line.length(); h++) {
					if (line.charAt(h) == '░')
						lcount++;
				}
				if ((double) lcount / line.length() < 0.825)
					b.append(line + "\n");
			}
			int count = 0;
			for (int i = 0; i < b.length(); i++) {
				if (b.charAt(i) == '▓')
					count++;
			}
			if ((double) count / b.length() > 0.5)
				continue;
			else
				return b;
		}
		return null;
	}
	boolean send(String t) throws Exception {
		//System.out.println("send invoked (t = " + t + ")");
		URL u = new URL("http://front1.omegle.com/recaptcha");
		HttpURLConnection c = (HttpURLConnection) u.openConnection();
		c.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux i686; rv:22.0) Gecko/20120101 Firefox/22.0");
		c.setDoOutput(true);
		OutputStreamWriter w = new OutputStreamWriter(c.getOutputStream());
		w.write("id=" + id + "&challenge=" + challenge + "&response=" + java.net.URLEncoder.encode(t));
		w.flush();
		w.close();
		BufferedReader r;
		try {
			r = new BufferedReader(new InputStreamReader(c.getInputStream()));
		} catch(FileNotFoundException e) {
			r = new BufferedReader(new InputStreamReader(c.getErrorStream()));
			System.out.println("CaptchaDialog 404");
		}
		boolean win = false;
		{ String line; while((line = r.readLine()) != null) {
				if(line.contains("win"))
					win = true;
				System.out.println("line: " + line);
			}
		}
		r.close();
		return win;
	}
	
	public String getArt() {
		return art;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println(printAsskey("http://www.google.com/recaptcha/api/image?c=03AHJ_Vuvcy6Ua5lxlS3Je_oVvTJ_4CBlMa7GzgiUFi6bsU8CID1D0Gb2SEVDKOpCWmvv__hZOCJVptIafvHlb3DqrIhghajIKAU83ZFm2fSMARs8xQ_zN4So_vYaC4Q0JOhvfzRicEFepLtpN8YQ5F_UgWR7M8Z1cSw"));
	}
}
