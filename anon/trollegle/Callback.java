package anon.trollegle;

public interface Callback {
    void callback(UserConnection user, String action, String parameter);
}
